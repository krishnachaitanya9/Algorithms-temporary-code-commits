import math
def floydwarshall(graph):
    dist = {}
    way = {}
    for u in graph:
        dist[u] = {}
        way[u] = {}
        for v in graph:
            dist[u][v] = 0
            way[u][v] = 0
        for neighbor in graph[u]:
            dist[u][neighbor] = graph[u][neighbor]
            if dist[u][neighbor] < math.inf:
                way[u][neighbor] = str(u)+'-'


    for t in graph:
        for u in graph:
            for v in graph:
                # if u != t and t != v:
                newdist = dist[u][t] + dist[t][v]
                if newdist < dist[u][v]:
                    dist[u][v] = newdist
                    way[u][v] = way[u][t] + way[t][v]
                if u == v and dist[u][v]<0:
                    return(way[u][v])


    # return dist,way

if __name__ == '__main__':
    INF = math.inf

    graph = { 1:{1:INF, 2:1,   3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:18},
              2:{1:INF, 2:INF, 3:2,   4:4,   5:INF,  6:INF, 7:INF, 8:INF, 9:INF},
              3:{1:INF, 2:INF, 3:INF, 4:INF, 5:INF,  6:6,   7:3,   8:INF, 9:INF},
              4:{1:INF, 2:INF, 3:INF, 4:INF, 5:5,    6:INF, 7:INF, 8:INF, 9:INF},
              5:{1:INF, 2:-12,  3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:INF},
              6:{1:INF, 2:INF, 3:-20, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:INF},
              7:{1:7,  2:INF, 3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:18,  9:INF},
              8:{1:INF, 2:INF, 3:INF, 4:INF, 5:INF,  6:INF, 7:-19, 8:INF, 9:INF},
              9:{1:19, 2:INF, 3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:INF}
    }
    # graph = {1: {1: INF, 2: 1, 3: INF, 4: INF, 5: INF, 6: INF, 7: INF},
    #          2: {1: INF, 2: INF, 3: 3, 4: INF, 5: INF, 6: INF, 7: INF},
    #          3: {1: INF, 2: INF, 3: INF, 4: INF, 5: 4, 6: -2, 7: INF},
    #          4: {1: INF, 2: INF, 3: INF, 4: INF, 5: INF, 6: INF, 7: INF},
    #          5: {1: INF, 2: INF, 3: INF, 4: INF, 5: INF, 6: INF, 7: INF},
    #          6: {1: INF, 2: INF, 3: INF, 4: INF, 5: INF, 6: INF, 7: -15},
    #          7: {1: INF, 2: 1, 3: INF, 4: INF, 5: INF, 6: INF, 7: INF}
    #          }


    print(len(floydwarshall(graph).split('-'))-1)

