
def cyclecheck(g):
    path = set()
    def visitnode(node):
        path.add(node)
        # for adjacent in g.get(node, ()):
        for adjacent in g[node]:
            if adjacent in path or visitnode(adjacent):
                return True
        path.remove(node)
        return False
    bool_array = [visitnode(v) for v in g]
    for boolean in bool_array:
        if boolean == True:
            return True
        else:
            continue
    return False





graph = {'1': ['2'],
         '2': ['3','6'],
         '3': [],
         '4': ['2'],
         '5': ['4'],
         '6': ['4']
         
         }

# print(dfs(graph, '1'))
print(cyclecheck(graph))