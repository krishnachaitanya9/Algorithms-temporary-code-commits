import math

global arbitrage_cycles
arbitrage_cycles = {}

def find_path(path_string, elem):
    global arbitrage_cycles
    path_list = path_string.split('-')[:-1]
    paths = {}
    index1 = 0
    for index,i in enumerate(path_list):
        if path_list[index]==elem:
            if index > 0:
                paths['-'.join(path_list[index1:index])] = 1
                index1 = index
    for individual_path in paths:
        individual_path_list = individual_path.split('-')
        individual_cycle = []
        for elem in individual_path_list:
            if elem in individual_cycle:
                index = individual_cycle.index(elem)
                individual_cycle = individual_cycle[:index+1]
            else:
                individual_cycle.append(elem)

        arbitrage_cycles['-'.join(individual_cycle)]=1
def floydwarshall(graph):
    dist = {}
    way = {}
    for u in graph:
        dist[u] = {}
        way[u] = {}
        for v in graph:
            dist[u][v] = 0
            way[u][v] = 0
        for neighbor in graph[u]:
            dist[u][neighbor] = graph[u][neighbor]
            if dist[u][neighbor] < math.inf:
                way[u][neighbor] = str(u)+'-'


    for t in graph:
        # given dist u to v, check if path u - t - v is shorter
        for u in graph:
            for v in graph:
                if u != t and t != v:
                    newdist = dist[u][t] * dist[t][v]
                    if newdist < dist[u][v]:
                        dist[u][v] = newdist
                        way[u][v] = way[u][t] + way[t][v]

    return dist,way





if __name__ == '__main__':
    INF = math.inf

    graph = { 1:{1:INF, 2:1,   3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:9},
              2:{1:INF, 2:INF, 3:2,   4:4,   5:INF,  6:INF, 7:INF, 8:INF, 9:INF},
              3:{1:INF, 2:INF, 3:INF, 4:INF, 5:INF,  6:6,   7:3,   8:INF, 9:INF},
              4:{1:INF, 2:INF, 3:INF, 4:INF, 5:5,    6:INF, 7:INF, 8:INF, 9:INF},
              5:{1:INF, 2:12,  3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:INF},
              6:{1:INF, 2:INF, 3:20, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:INF},
              7:{1:7,  2:INF, 3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:18,  9:INF},
              8:{1:INF, 2:INF, 3:INF, 4:INF, 5:INF,  6:INF, 7:-19, 8:INF, 9:INF},
              9:{1:19, 2:INF, 3:INF, 4:INF, 5:INF,  6:INF, 7:INF, 8:INF, 9:INF}
    }

    distance,waypath = floydwarshall(graph)
    for elem in graph:
        find_path(waypath[elem][elem],str(elem))
    # print(distance)
    print(arbitrage_cycles)

