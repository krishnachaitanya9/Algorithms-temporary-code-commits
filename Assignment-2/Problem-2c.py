from random import getrandbits

def makeOneThirdCoin():
    returnFalse = False
    rangevariable = 1000
    for i in range(rangevariable):
        if i==0:
            if bool(getrandbits(1)):
                returnFalse = True
                break
            else:
                continue
        elif i%2 == 1:
            if bool(getrandbits(1)):
                continue
            else:
                break
        elif i%2 == 0:
            if bool(getrandbits(1)):
                returnFalse = True
                break
            else:
                continue
    if i==rangevariable-1:
        return True
    elif not returnFalse:
        return True
    else:
        return False

count = 0
t = 0
nTrials = 10000000
for i in range(nTrials):
    b = makeOneThirdCoin()
    if (b):
        count = count + 1
print('Count = ', count, ' probability test = ', count/nTrials)

