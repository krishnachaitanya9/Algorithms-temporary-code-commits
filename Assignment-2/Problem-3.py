#########################################################################################################
from random import randint
def simulateCouponCollection(n):
    # Your code here.
    def check_if_array_is_alltrue(k):
        for i in k:
            if i == True:
                continue
            else:
                return False
        return True

    b =[]
    for i in range(n):  b.append(False)
    count = 0
    while not check_if_array_is_alltrue(b):
        b[randint(0,n-1)] = True
        count += 1
    return count
#########################################################################################################
def runCouponCollectionExperiment(nMax):
    returnData = {}
    # Your code here
    n = list(range(10, nMax, 10))
    for i in n:
        print('i = ',i)
        average_taking_array = []
        for _ in range(1000):
            average_taking_array.append(simulateCouponCollection(i))
        returnData[i] = (sum(average_taking_array)/len(average_taking_array))
    return returnData
################################################################################################
mymap = runCouponCollectionExperiment(200)

import matplotlib.pyplot as plt
import math
def plotMap(m, nMax):
    n = list(range(10, nMax, 10))
    v = []
    nlogn = []
    for i in n:
        nlogn.append( 1.1* i * math.log(i))
        if i in m:
            v.append(m[i])
        else:
            print('Warning: no data for n = ', i)
            v.append(0)
    plt.plot(n, v, 'go-', label='Avg. Steps to Collect all Coupons', linewidth=2)
    plt.plot(n, nlogn, 'r--', label='n log(n)', linewidth=2)
    plt.xlabel("n")
    plt.ylabel("# steps to collect all coupons")
    plt.show()
plotMap(mymap, 200)

def runHistogram(n, numTrials):
    lst = []
    for i in range(numTrials):
        lst.append(simulateCouponCollection(n))
    return lst
lst = runHistogram(500, 5000)
def plotHistogramFromList(lst):
    plt.hist(lst, density=True, bins=50)
    plt.ylabel('Probability')
    plt.xlabel('# steps to collect all coupons')
    plt.show()
plotHistogramFromList(lst)