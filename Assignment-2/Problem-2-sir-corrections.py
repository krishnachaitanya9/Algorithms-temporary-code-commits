'''
why its not working, read Problem-2a-approach1 comments.
'''
from random import getrandbits

RUNS = 1000000
global ct_twice
global ct_four
global ct_six
global ct_eight
global ct_ten
ct_twice = 0
ct_four = 0
ct_six = 0
ct_eight = 0
ct_ten = 0
def coin_flip():
    return bool(getrandbits(1))

def flip_biased_coin_twice():
    return coin_flip() and coin_flip()
def flip_biased_coin_four_times():
    return coin_flip() and coin_flip() and coin_flip() and coin_flip()
def flip_biased_coin_six_times():
    return coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip()
def flip_biased_coin_eight_times():
    return coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip()
def flip_biased_coin_ten_times():
    return coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip()
def makeOneThirdCoin_experiment():
    global ct_twice
    global ct_four
    global ct_six
    global ct_eight
    global ct_ten
    f_twice = flip_biased_coin_twice()
    f_four = flip_biased_coin_four_times()
    f_six = flip_biased_coin_six_times()
    f_eight = flip_biased_coin_eight_times()
    f_ten = flip_biased_coin_ten_times()
    if f_twice:
        ct_twice += 1
    if f_four:
        ct_four += 1
    if f_six:
        ct_six += 1
    if f_eight:
        ct_eight += 1
    if f_ten:
        ct_ten += 1
    return f_twice | f_four | f_six | f_eight | f_ten

def makeOneThirdCoin():
    f_twice = flip_biased_coin_twice()
    f_four = flip_biased_coin_four_times()
    f_six = flip_biased_coin_six_times()
    f_eight = flip_biased_coin_eight_times()
    f_ten = flip_biased_coin_ten_times()
    if f_twice:
        return True
    elif f_four:
        return True
    elif f_six:
        return True
    elif f_eight:
        return True
    elif f_ten:
        return True
    else:
        return False
count = 0
t = 0
nTrials = 10000000
for i in range(nTrials):
    b = makeOneThirdCoin_experiment()
    if (b):
        count = count + 1
print('Count = ', count, ' probability test = ', count/nTrials)
print('---------------------------------------------------')
print('ct_twice ',ct_twice)
print('ct_four ',ct_four)
print('ct_six ',ct_six)
print('ct_eight ',ct_eight)
print('ct_ten ',ct_ten)
print('Total ',ct_twice+ct_four+ct_six+ct_eight+ct_ten)