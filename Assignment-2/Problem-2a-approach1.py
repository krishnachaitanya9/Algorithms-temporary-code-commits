## Here I am counting whenever each case(flip_biased_coin_twice or flip_biased_coin_four_times or flip_biased_coin_six_times)
## and adding it up. It's mathematically correct way. Need to ask professor if I am doing right.
'''
I am confusing in between mutually exclusive events and independent events, which shouldn't be done.
Prof Sriram pointed it out.
Thank You.
'''

from random import getrandbits

RUNS = 1000000

def coin_flip():
    return bool(getrandbits(1))

def flip_biased_coin_twice():
    return coin_flip() and coin_flip()
def flip_biased_coin_four_times():
    return coin_flip() and coin_flip() and coin_flip() and coin_flip()
def flip_biased_coin_six_times():
    return coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip() and coin_flip()

ct = 0
for i in range(RUNS):
    if flip_biased_coin_twice():
        ct += 1
flip_twice = ct

ct = 0
for i in range(RUNS):
    if flip_biased_coin_four_times():
        ct += 1
flip_four = ct

ct = 0
for i in range(RUNS):
    if flip_biased_coin_six_times():
        ct += 1
flip_six = ct

print(ct, RUNS, float(flip_twice+flip_four+flip_six)/RUNS, float(21/64))

ct = 0
for i in range(RUNS):
    if flip_biased_coin_twice() or flip_biased_coin_four_times() or flip_biased_coin_six_times():
        ct += 1
print('combined probability ', float(ct)/RUNS)