from random import getrandbits
def coin_flip():
    return bool(getrandbits(1))
def two_coin_flip():
    return coin_flip() or coin_flip()
def three_coin_flip():
    return coin_flip() or coin_flip() or coin_flip()
iters = 1000000
count = 0
for i in range(iters):
    if coin_flip() and two_coin_flip() and three_coin_flip():
        count += 1
print('Probability calculated = ',float(count)/iters)