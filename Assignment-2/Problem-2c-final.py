from random import getrandbits
global coin_tosses
coin_tosses = 0
def makeOneThirdCoin_previous():
    rangevariable = 10
    def coin_flip():
        return bool(getrandbits(1))
    itervariable = 0
    while itervariable <= rangevariable:
        coin_one = coin_flip()
        coin_two = coin_flip()
        if coin_one & coin_two:
            return True
        elif coin_one | coin_two:
            return False
        itervariable += 1
    return False



'''This is Sir's infinite loop method. Chill Fellas. This idea struck when I was talking to him LOL'''

def makeOneThirdCoin():
    global coin_tosses
    while 1:
        coin_one_result = bool(getrandbits(1))
        coin_two_result = bool(getrandbits(1))
        coin_tosses += 2
        if coin_one_result & coin_two_result:
            return True
        elif coin_one_result | coin_two_result:
            return False






count = 0
t = 0
nTrials = 10000000
for i in range(nTrials):
    b = makeOneThirdCoin()
    if (b):
        count = count + 1
print('Count = ', count, ' probability test = ', count/nTrials)
print('Coin_Tosses = ',float(coin_tosses)/nTrials)