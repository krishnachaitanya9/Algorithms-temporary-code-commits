from random import getrandbits
global coin_tosses
coin_tosses = 0
def makeOneThirdCoin():
    global coin_tosses
    rangevariable = 2
    def coin_flip():
        return bool(getrandbits(1))
    i = 0
    while i<=rangevariable:
        a = coin_flip()
        b = coin_flip()
        coin_tosses += 1
        if a & b:
            return True
        elif a | b:
            return False
        i += 1
    return False



count = 0
t = 0
nTrials = 10000000
for i in range(nTrials):
    b = makeOneThirdCoin()
    if (b):
        count = count + 1
print('Count = ', count, ' probability test = ', count/nTrials)
print('Coin Tosses = ',float(coin_tosses)/nTrials)