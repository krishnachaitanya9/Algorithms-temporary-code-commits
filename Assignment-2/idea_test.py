from random import getrandbits
def makeOneThirdCoin():
    rangevariable = 10
    def coin_flip():
        return bool(getrandbits(1))
    i = 0
    while i<=rangevariable:
        a = coin_flip()
        b = coin_flip()
        if a & b:
            return True
        elif a | b:
            return False
        i += 1
    return False



count = 0
t = 0
nTrials = 10000000
for i in range(nTrials):
    b = makeOneThirdCoin()
    if (b):
        count = count + 1
print('Count = ', count, ' probability test = ', count/nTrials)