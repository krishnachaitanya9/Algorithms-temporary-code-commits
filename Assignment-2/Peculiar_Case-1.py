## As in the 17th line previous_result is initialized to True, it doesn't go into makeOneThirdCoin() function.
# True or Anything, will always return true. Be careful

from random import getrandbits
import math

def makeOneThirdCoin():
    denominator_2_power = 2
    previous_result = True
    def get_coin_flips(n):
        flip_coin_result = True
        if n > 0:
            if n > 1:
                flip_coin_result = get_coin_flips(n - 1) and bool(getrandbits(1))
            else:
                return flip_coin_result and bool(getrandbits(1))

    while denominator_2_power < 10:
        previous_result = previous_result or get_coin_flips(int(math.pow(2,denominator_2_power)))
        denominator_2_power += 2
    return previous_result





count = 0
t = 0
nTrials = 10000000
for i in range(nTrials):
    b = makeOneThirdCoin()
    if (b):
        count = count + 1
print('Count = ', count, ' probability test = ', count/nTrials)




