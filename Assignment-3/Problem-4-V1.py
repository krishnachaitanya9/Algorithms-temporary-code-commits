# test 1
import math
from random import sample

def maxSubArray(a):
    # Code here.
    # Just use a single for loop that iterates over
    # the array in forward or reverse direction, as you please
    min_element_encountered = a[0]
    min_element_index = 0
    max_element_encountered = a[0]
    max_element_index = 0
    max_difference = 0
    for index, a_element in enumerate(a):
        if a_element - min_element_encountered < 0:
            min_element_encountered = a_element
            min_element_index = index
        elif a_element - min_element_encountered > max_difference:
            max_difference = a_element - min_element_encountered
            max_element_encountered = a_element
            max_element_index = index
        else:
            pass

    # print(a)
    # print('min Element: ',min_element_encountered,' With Index: ',min_element_index)
    # print('max Element: ', max_element_encountered , ' With Index: ', max_element_index)
    return max_difference if max_difference>0 else -math.inf


a = [3, 1, 0, 3, 2, 1, 4, 9, 10, 8, 5, 7, 9]
# print(maxSubArray(a))
assert maxSubArray(a) == 10, 'Test 1 failed'

# test 2

b = [10, 4, 5, 1, 2, 7, 8, 1, 0, -10, 10, -5, 19, 231, 11, -55]
# print(maxSubArray(b))
assert maxSubArray(b) == 241, 'Test 2 failed'

# test 3

c = [10, 2, -1, -5, -4]

# print(maxSubArray(c))
assert maxSubArray(c) == 1, 'Test 3 failed'


# test 4

def naiveMaxSubArray(a):
    n = len(a)
    maxSoFar = -math.inf
    for i in range(n - 1):
        for j in range(i + 1, n):
            if (a[i] < a[j] and maxSoFar < (a[j] - a[i])):
                maxSoFar = a[j] - a[i]
    return maxSoFar


def testMaxSubArray(nTests, testSize):
    for i in range(nTests):
        a = sample(range(-1 * nTests, nTests), testSize)
        j = naiveMaxSubArray(a)
        l = maxSubArray(a)
        if (j != l):
            print('Failed for array: ', (a), '\n Expected: ', j, ' Obtained: ', l)
            return
    print(nTests, 'tests passed!')


testMaxSubArray(1000, 5)
testMaxSubArray(1000, 10)
testMaxSubArray(1000, 30)