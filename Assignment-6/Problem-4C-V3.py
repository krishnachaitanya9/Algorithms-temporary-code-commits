def minSubsetDifference_Memoize(N, s_list):
    max_weight = N
    lenv = len(s_list)
    m = [[0 for k in range(max_weight + 1)] for i in range(len(s_list) + 1)]

    for i in range(lenv):
        m[i][0] = 0

    for i in range(max_weight + 1):
        m[0][i] = 0

    start = min(s_list)
    for item in range(1, lenv + 1):
        for max_w in range(start, max_weight + 1):
            w_i = s_list[item - 1]

            if w_i > max_w:
                m[item][max_w] = m[item - 1][max_w]
            else:
                m[item][max_w] = max(m[item - 1][max_w], m[item - 1][max_w - w_i] + w_i)

    return max_weight - m[lenv][max_weight]




# Code for testing your solution
# DO NOT EDIT
print(minSubsetDifference_Memoize(15, [1, 2, 3, 4, 5, 10])) # Should be 0
print(minSubsetDifference_Memoize(26, [1, 2, 3, 4, 5, 10])) # should be 1
print(minSubsetDifference_Memoize(23, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference_Memoize(18, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference_Memoize(9, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference_Memoize(457, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 1
print(minSubsetDifference_Memoize(512, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 0
print(minSubsetDifference_Memoize(616, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 1