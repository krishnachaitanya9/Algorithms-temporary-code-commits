from math import inf
import copy
global temp_dict
temp_dict = {}
def minCoursesForJane_Solution(n):  # Assume that j = 1 is always the starting point
    global temp_dict
    j = 1
    # Base Case-1
    if n in temp_dict:
        return len(temp_dict[n]),temp_dict[n]
    else:
        while j <= n:
            # Base Case -2
            if n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
                if n not in temp_dict:
                    temp_dict[n] = [n-j]
                return 1,temp_dict[n]

            else:
                step1 , list1 = minCoursesForJane_Solution(n - 11)
                step2, list2 = minCoursesForJane_Solution(n - 5)
                step3, list3 = minCoursesForJane_Solution(n - 4)
                step4, list4 = minCoursesForJane_Solution(n - 1)
                min_list = [1+step1,1+step2,1+step3,1+step4]
                min_value = min(min_list)
                min_index = min_list.index(min_value)
                if n not in temp_dict:
                    if min_index==0:
                        templist1 = copy.copy(list1)
                        templist1.append(11)
                        temp_dict[n] = templist1
                    elif min_index==1:
                        templist2 = copy.copy(list2)
                        templist2.append(5)
                        temp_dict[n] = templist2
                    elif min_index==2:
                        templist3 = copy.copy(list3)
                        templist3.append(4)
                        temp_dict[n] = templist3
                    else:
                        templist4 = copy.copy(list4)
                        templist4.append(1)
                        temp_dict[n] = templist4
                return len(temp_dict[n]),temp_dict[n]

        if j > n:
            return inf,[]


print(minCoursesForJane_Solution(9))  # should be 2, [4, 4]
print(minCoursesForJane_Solution(13)) # should be 2, [1, 11]
print(minCoursesForJane_Solution(19)) # should be 4, [1, 1, 5, 11]
print(minCoursesForJane_Solution(34)) # should be 3, [11, 11, 11]
print(minCoursesForJane_Solution(43)) # should be 5, [4, 5, 11, 11, 11]
# print(T)
