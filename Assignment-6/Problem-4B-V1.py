import copy
from math import inf
import itertools
global min_diff
min_diff = inf
global subset_list
subset_list = []
def minSubsetDifference_recursive(N, s_list,splice_index=0,firsttime=True):
    # n is the target number
    # s_list is a list of elements in the set S
    # Your code here
    global min_diff
    global subset_list
    if firsttime:
        min_diff = inf
        subset_list = []
    if min_diff == 0:
        return min_diff,subset_list
    else:
        if splice_index == len(s_list)-1:
            for i in s_list:
                if N - i < min_diff and N - i >=0:
                    min_diff = N - i
                    subset_list = [i]
            return min_diff,subset_list
        elif splice_index == 0:
            current_diff = N - sum(s_list)
            if current_diff>=0 and current_diff<min_diff:
                subset_list = copy.copy(s_list)
                min_diff = current_diff
            return minSubsetDifference_recursive(N,s_list,splice_index+1,False)
        else:
            for every_list in list(itertools.permutations(s_list)):
                current_diff = N - sum(every_list[splice_index:])
                if current_diff>=0 and current_diff < min_diff:
                    subset_list = copy.copy(every_list[splice_index:])
                    min_diff = current_diff
            return minSubsetDifference_recursive(N,s_list,splice_index+1,False)



print(minSubsetDifference_recursive(15, [1, 2, 3, 4, 5, 10])) # Should be zero
print(minSubsetDifference_recursive(26, [1, 2, 3, 4, 5, 10])) # should be 1
print(minSubsetDifference_recursive(23, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference_recursive(18, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference_recursive(9, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference_recursive(457, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 1
print(minSubsetDifference_recursive(512, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 0
print(minSubsetDifference_recursive(616, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 1