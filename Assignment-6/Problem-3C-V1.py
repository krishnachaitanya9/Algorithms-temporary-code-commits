from math import inf
import copy
global temp_dict
temp_dict = {}
global Matrix
def minCoursesWithEnergyBudget_Solution(e, n,highest=True,runfirsttime=True):  # Assume that j = 1 is always the starting point
    global Matrix
    if runfirsttime:
        w, h = e,n
        Matrix = [[0 for x in range(w+2)] for y in range(h+2)]
    j = 1
    if n>=0 and e>=0:
        if Matrix[n][e] != 0:
            return len(Matrix[n][e]),Matrix[n][e]
        while j < n:
            # Base Case - 2
            if n%7==2 and not highest:
                return inf,[]
            elif n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
                if n - j == 11:
                    if e-7 > 0:
                        Matrix[n][e] = [n - j]
                        return 1,Matrix[n][e]
                    else:
                        return inf,[]
                elif n - j == 5:
                    if e-3 > 0:
                        Matrix[n][e] = [n - j]
                        return 1,Matrix[n][e]
                    else:
                        return inf,[]
                elif n - j == 4:
                    if e-2 > 0:
                        Matrix[n][e] = [n - j]
                        return 1,Matrix[n][e]
                    else:
                        return inf,[]
                elif n - j == 1:
                    if e-1 > 0:
                        Matrix[n][e] = [n - j]
                        return 1,Matrix[n][e]
                    else:
                        return inf,[]

            else:
                step1, list1 = minCoursesWithEnergyBudget_Solution(e-7, n - 11, False, False)
                step2, list2 = minCoursesWithEnergyBudget_Solution(e-3, n - 5, False, False)
                step3, list3 = minCoursesWithEnergyBudget_Solution(e-2, n - 4, False, False)
                step4, list4 = minCoursesWithEnergyBudget_Solution(e-1, n - 1, False, False)
                min_list = [1+step1,1+step2,1+step3,1+step4]
                min_value = min(min_list)
                min_index = min_list.index(min_value)
                if step1==inf and step2 == inf and step3 == inf and step4 == inf:
                    return inf,[]
                else:
                    if min_index == 0:
                        templist1 = copy.copy(list1)
                        templist1.append(11)
                        Matrix[n][e] = templist1
                    elif min_index == 1:
                        templist2 = copy.copy(list2)
                        templist2.append(5)
                        Matrix[n][e] = templist2
                    elif min_index == 2:
                        templist3 = copy.copy(list3)
                        templist3.append(4)
                        Matrix[n][e] = templist3
                    elif min_index == 3:
                        templist4 = copy.copy(list4)
                        templist4.append(1)
                        Matrix[n][e] = templist4
                    return min_value,Matrix[n][e]

        if j > n:
            return inf,[]
    else:
        return inf, []




# test code do not edit
print(minCoursesWithEnergyBudget_Solution(25, 10)) # must be 2, [4,5]
print(minCoursesWithEnergyBudget_Solution(25, 6)) # must be 1, [5]
print(minCoursesWithEnergyBudget_Solution(25, 30)) # must be 5, [4, 5, 4, 5, 11]
print(minCoursesWithEnergyBudget_Solution(16, 30)) # must be 7, [4, 5, 4, 4, 4, 4, 4]
print(minCoursesWithEnergyBudget_Solution(18, 31)) # must be 7, [4, 5, 4, 4, 4, 4, 5]
print(minCoursesWithEnergyBudget_Solution(22, 38)) # must be 7,  [4, 5, 4, 4, 4, 5, 11]
print(minCoursesWithEnergyBudget_Solution(32, 55)) # must be 11, [4, 5, 4, 4, 4, 4, 5, 4, 4, 11, 5]
print(minCoursesWithEnergyBudget_Solution(35, 60)) # must be 12, [4, 5, 4, 4, 4, 4, 5, 4, 4, 11, 5, 5]