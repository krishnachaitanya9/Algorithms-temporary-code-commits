def knapsack(v: list, w: list, max_weight: int):
    rows = len(v) + 1
    cols = max_weight + 1

    # adding dummy values as later on we consider these values as indexed from 1 for convinence
    v = [0] + v[:]
    w = [0] + w[:]

    # row : values , #col : weights
    dp_array = [[0 for i in range(cols)] for j in range(rows)]

    # 0th row and 0th column have value 0

    # values
    for i in range(1, rows):
        # weights
        for j in range(1, cols):
            # if this weight exceeds max_weight at that point
            if j - w[i] < 0:
                dp_array[i][j] = dp_array[i - 1][j]

            # max of -> last ele taken | this ele taken + max of previous values possible
            else:
                dp_array[i][j] = j - max(dp_array[i - 1][j], v[i] + dp_array[i - 1][j - w[i]])

    # return dp_array[rows][cols]  : will have the max value possible for given wieghts

    values_chosen = []
    i = rows - 1
    j = cols - 1

    # Get the items to be picked
    while i > 0 and j > 0:

        # ith element is added
        if dp_array[i][j] != dp_array[i - 1][j]:
            # add the value
            values_chosen.append(w[i])
            # decrease the weight possible (j)
            j = j - v[i]
            # go to previous row
            i = i - 1

        else:
            i = i - 1
    sub_ans= max_weight - sum(values_chosen)

    return dp_array[rows - 1][cols - 1], values_chosen, sub_ans


values = [1, 1, 1, 1, 1]
weights = [1, 2, 3, 4, 5, 10]
max_weight = 9

max_value, values_chosen, sub_ans = knapsack(values, weights, max_weight)

print("The max value possible is")
print(max_value)

print("The values chosen for these are")
print(' '.join(str(x) for x in values_chosen))

print('Subtraction Answer',sub_ans)