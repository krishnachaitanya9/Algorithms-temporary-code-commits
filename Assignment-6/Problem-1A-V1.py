#Version of Greedy Algorithm
from math import inf
def minCoursesForJane(j, n):
    # Your code here
    # Must return a number
    global counter
    counter = 0
    def minCourses(x,y):
        global counter
        diffe = y-x
        if diffe > 0:
            if diffe >= 11:
                counter += 1
                minCourses(x+11,y)
            elif diffe >= 5:
                counter += 1
                minCourses(x+5,y)
            elif diffe >= 4:
                counter += 1
                minCourses(x+4,y)
            else:
                counter += 1
                minCourses(x+1,y)
    minCourses(j,n)
    return counter


## Test Code: Do not edit
print(minCoursesForJane(1, 9)) # should be 2
print(minCoursesForJane(1, 13)) # should be 2
print(minCoursesForJane(1, 19)) # should be 4
print(minCoursesForJane(1, 34)) # should be 3
print(minCoursesForJane(1, 43)) # should be 5