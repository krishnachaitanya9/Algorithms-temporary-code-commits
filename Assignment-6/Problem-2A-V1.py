from math import inf
global temp_dict
temp_dict = {}
def minCoursesForJaneAvoidKK(j,n,highest=True):  # Assume that j = 1 is always the starting point
    global temp_dict
    # Base Case-1
    # if n in temp_dict:
    #     return temp_dict[n]
    while j <= n:
        if n==j:
            if n not in temp_dict:
                temp_dict[n] = 0
            return 0
        # Base Case -2
        elif n%7==2 and not highest:
            return inf
        elif n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
            if n not in temp_dict:
                temp_dict[n] = 1
            return 1
        else:
            min_list = [1+minCoursesForJaneAvoidKK(1,n - 11,False),1+minCoursesForJaneAvoidKK(1,n - 5,False),
                        1+minCoursesForJaneAvoidKK(1,n - 4,False),1+minCoursesForJaneAvoidKK(1,n - 1,False)]
            min_value = min(min_list)
            if n not in temp_dict:
                temp_dict[n] = min_value
            return min_value

    if j > n:
        return inf


## Test Code: Do not edit

print(minCoursesForJaneAvoidKK(1, 9)) # should be 2
print(minCoursesForJaneAvoidKK(1, 13)) # should be 2
print(minCoursesForJaneAvoidKK(1, 19)) # should be 4
print(minCoursesForJaneAvoidKK(1, 34)) # should be 5
print(minCoursesForJaneAvoidKK(1, 43)) # should be 5
print(minCoursesForJaneAvoidKK(1, 55)) # should be 6