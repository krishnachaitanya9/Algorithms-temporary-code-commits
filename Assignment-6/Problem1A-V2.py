#Version of Greedy Algorithm
from math import inf
def minCoursesForJane(j,n):
    # Base Case-1
    while j <= n:
        if n==j:
            return 0
        # Base Case -2
        elif j - n == 11 or j - n == 5 or j - n == 4 or j - n == 1:
            return 1
        else:
            return min(1+minCoursesForJane(j+11,n),1+minCoursesForJane(j+5,n),1+minCoursesForJane(j+4,n),1+minCoursesForJane(j+1,n))
    if j > n:
        return inf

## Test Code: Do not edit
print(minCoursesForJane(1, 10)) # should be 2
# print(minCoursesForJane(1, 13)) # should be 2
# print(minCoursesForJane(1, 19)) # should be 4
# print(minCoursesForJane(1, 34)) # should be 3
# print(minCoursesForJane(1, 43)) # should be 5