from math import inf
global temp_dict
temp_dict = {}

def minCoursesWithEnergyBudget(j, e, n,highest=True):  # Assume that j = 1 is always the starting point
    global temp_dict
    while j < n:
        # Base Case -2
        if n%7==2 and not highest:
            return inf
        elif n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
            if n - j == 11:
                if e-7 > 0:
                    return 1
                else:
                    return inf
            elif n - j == 5:
                if e-3 > 0:
                    return 1
                else:
                    return inf
            elif n - j == 4:
                if e-2 > 0:
                    return 1
                else:
                    return inf
            elif n - j == 1:
                if e-1 > 0:
                    return 1
                else:
                    return inf

        else:
            min_list = [1+minCoursesWithEnergyBudget(1,e-7,n - 11,False), 1+minCoursesWithEnergyBudget(1,e-3,n - 5,False),
                        1+minCoursesWithEnergyBudget(1,e-2,n - 4,False), 1+minCoursesWithEnergyBudget(1,e-1,n - 1,False)]
            min_value = min(min_list)
            if n not in temp_dict:
                temp_dict[n] = min_value
            return min_value

    if j > n:
        return inf


# test code do not edit
print(minCoursesWithEnergyBudget(1, 25, 10)) # must be 2
print(minCoursesWithEnergyBudget(1, 25, 6)) # must be 1
print(minCoursesWithEnergyBudget(1, 25, 30)) # must be 5
print(minCoursesWithEnergyBudget(1, 16, 30)) # must be 7
print(minCoursesWithEnergyBudget(1, 18, 31)) # must be 7
print(minCoursesWithEnergyBudget(1, 22, 38)) # must be 7
print(minCoursesWithEnergyBudget(1, 32, 55)) # must be 11
print(minCoursesWithEnergyBudget(1, 35, 60)) # must be 12