from math import inf
global Matrix

def minCoursesWithEnergyBudget_Memoize(e, n,highest=True,runfirsttime=True):  # Assume that j = 1 is always the starting point
    global Matrix
    if runfirsttime:
        w, h = e,n
        Matrix = [[0 for x in range(w+1)] for y in range(h+1)]
    j = 1
    if Matrix[n][e] != 0:
        return Matrix[n][e]
    while j < n:
        # Base Case - 2
        if n%7==2 and not highest:
            return inf
        elif n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
            if n - j == 11:
                if e-7 > 0:
                    Matrix[n][e] = 1
                    return 1
                else:
                    return inf
            elif n - j == 5:
                if e-3 > 0:
                    Matrix[n][e] = 1
                    return 1
                else:
                    return inf
            elif n - j == 4:
                if e-2 > 0:
                    Matrix[n][e] = 1
                    return 1
                else:
                    return inf
            elif n - j == 1:
                if e-1 > 0:
                    Matrix[n][e] = 1
                    return 1
                else:
                    return inf

        else:
            min_list = [1+minCoursesWithEnergyBudget_Memoize(e-7,n - 11,False,False), 1+minCoursesWithEnergyBudget_Memoize(e-3,n - 5,False,False),
                        1+minCoursesWithEnergyBudget_Memoize(e-2,n - 4,False,False), 1+minCoursesWithEnergyBudget_Memoize(e-1,n - 1,False,False)]
            min_value = min(min_list)
            min_index = min_list.index(min_value)
            if min_index == 0:
                Matrix[n][e] = min_value
            elif min_index == 1:
                Matrix[n][e] = min_value
            elif min_index == 2:
                Matrix[n][e] = min_value
            elif min_index == 3:
                Matrix[n][e] = min_value
            return min_value

    if j > n:
        return inf



# test code do not edit
print(minCoursesWithEnergyBudget_Memoize(25, 10)) # must be 2
print(minCoursesWithEnergyBudget_Memoize(25, 6)) # must be 1
print(minCoursesWithEnergyBudget_Memoize(25, 30)) # must be 5
print(minCoursesWithEnergyBudget_Memoize(16, 30)) # must be 7
print(minCoursesWithEnergyBudget_Memoize(18, 31)) # must be 7
print(minCoursesWithEnergyBudget_Memoize(22, 38)) # must be 7
print(minCoursesWithEnergyBudget_Memoize(32, 55)) # must be 11
print(minCoursesWithEnergyBudget_Memoize(35, 60)) # must be 12
