def minSubsetDifference(N, s_list):
    len_of_s = len(s_list)
    zero_array = [[0 for k in range(N + 1)] for i in range(len_of_s + 1)]
    path_taken = [[(0,0,0,".") for k in range(N + 1)] for i in range(len_of_s + 1)]

    for i in range(len_of_s):
        zero_array[i][0] = 0



    for i in range(N + 1):
        zero_array[0][i] = 0

    st = min(s_list)
    for item in range(1, len_of_s + 1):
        for w in range(st, N + 1):
            w_i = s_list[item - 1]

            if w_i > w:
                zero_array[item][w] = zero_array[item - 1][w]
                path_taken[item][w] = (item-1, w, s_list[item-1],"|")
            else:
                if(zero_array[item - 1][w]> zero_array[item - 1][w - w_i] + w_i):
                    zero_array[item][w] = zero_array[item - 1][w]
                    path_taken[item][w] = (item - 1, w, w,"|")
                else:
                    zero_array[item][w] = zero_array[item - 1][w - w_i] + w_i
                    path_taken[item][w] = (item - 1, w - w_i, w_i,".")
    path = []
    i = len_of_s
    j = N
    s = path_taken[i][j]
    total = path_taken[i][j][2]

    while(total>0):
        if(path_taken[i][j][3] == "up"):
            m = path_taken[i][j][0]
            n = path_taken[i][j][1]
            i = m
            j = n
            total = path_taken[i][j][2]
            pass
        else:
            path.append(path_taken[i][j][2])
            m = path_taken[i][j][0]
            n = path_taken[i][j][1]
            i = m
            j = n
            total = path_taken[i][j][2]
    return N - zero_array[len_of_s][N], path

# Code for testing your solution
# DO NOT EDIT
print(minSubsetDifference(15, [1, 2, 3, 4, 5, 10])) # Should be 0
print(minSubsetDifference(26, [1, 2, 3, 4, 5, 10])) # should be 1
print(minSubsetDifference(23, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference(18, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference(9, [1, 2, 3, 4, 5, 10])) # should be 0
print(minSubsetDifference(457, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 1
print(minSubsetDifference(512, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 0
print(minSubsetDifference(616, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413])) # should be 1