from math import inf
global temp_dict
temp_dict = {}
def minCoursesForJane_Memoize(n):  # Assume that j = 1 is always the starting point
    global temp_dict
    j = 1
    # Base Case-1
    if n in temp_dict:
        return temp_dict[n]
    while j <= n:
        if n==j:
            if n not in temp_dict:
                temp_dict[n] = 0
            return 0
        # Base Case -2
        elif n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
            if n not in temp_dict:
                temp_dict[n] = 1
            return 1
        else:
            min_list = [1+minCoursesForJane_Memoize(n - 11),1+minCoursesForJane_Memoize(n - 5),
                        1+minCoursesForJane_Memoize(n - 4),1+minCoursesForJane_Memoize(n - 1)]
            min_value = min(min_list)
            if n not in temp_dict:
                temp_dict[n] = min_value
            return min_value

    if j > n:
        return inf


print(minCoursesForJane_Memoize(9))  # should be 2, [4, 4]
print(temp_dict)
print(minCoursesForJane_Memoize(13)) # should be 2, [1, 11]
print(temp_dict)
print(minCoursesForJane_Memoize(19)) # should be 4, [1, 1, 5, 11]
print(temp_dict)
print(minCoursesForJane_Memoize(34)) # should be 3, [11, 11, 11]
print(temp_dict)
print(minCoursesForJane_Memoize(43)) # should be 5, [4, 5, 11, 11, 11]
print(temp_dict)
# print(T)
