from math import inf
global temp_dict
temp_dict = {}
def minCoursesForJaneAvoidKK_Memoize(n,highest=True):  # Assume that j = 1 is always the starting point
    global temp_dict
    # Base Case-1
    if n in temp_dict:
        return temp_dict[n]
    j = 1
    while j <= n:
        if n==j:
            if n not in temp_dict:
                temp_dict[n] = 0
            return 0
        # Base Case -2
        elif n%7==2 and not highest:
            return inf
        elif n - j == 11 or n - j == 5 or n - j == 4 or n - j == 1:
            if n not in temp_dict:
                temp_dict[n] = 1
            return 1
        else:
            min_list = [1+minCoursesForJaneAvoidKK_Memoize(n - 11,False),1+minCoursesForJaneAvoidKK_Memoize(n - 5,False),
                        1+minCoursesForJaneAvoidKK_Memoize(n - 4,False),1+minCoursesForJaneAvoidKK_Memoize(n - 1,False)]
            min_value = min(min_list)
            if n not in temp_dict:
                temp_dict[n] = min_value
            return min_value

    if j > n:
        return inf


## Test Code: Do not edit

print(minCoursesForJaneAvoidKK_Memoize(9)) # should be 2
print(minCoursesForJaneAvoidKK_Memoize(13)) # should be 2
print(minCoursesForJaneAvoidKK_Memoize(19)) # should be 4
print(minCoursesForJaneAvoidKK_Memoize(34)) # should be 5
print(minCoursesForJaneAvoidKK_Memoize(43)) # should be 5
print(minCoursesForJaneAvoidKK_Memoize(55)) # should be 6
print(minCoursesForJaneAvoidKK_Memoize(69)) # should be 8
print(minCoursesForJaneAvoidKK_Memoize(812)) # should be 83