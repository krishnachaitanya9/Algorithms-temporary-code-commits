import math
def function_cal(x,y):
    return x+math.pow(x,2)+math.pow(y,2)-y
n = 10
collision_track = 0
list_of_numbers = list(range(n))[1:]
_dict = {}
for j in list_of_numbers:
    for k in list_of_numbers:
        func_value = function_cal(j,k)
        if func_value not in _dict:
            _dict[func_value] = str(j)+' and '+str(k)+', '
        else:
            previous_stored_string = _dict[func_value]
            _dict[func_value] = previous_stored_string + str(j) + ' and ' + str(k) + ', '
            if len(previous_stored_string.split(','))==2:
                collision_track += 2
            else:
                collision_track += 1
ct = 0
line_number = 1
for keys,values in _dict.items():
    unique_elements_list = values.split(',')
    if len(unique_elements_list) > 2:
        print(str(line_number)+')',keys, ':', values)
        line_number += 1
        ct += 1

print('Length of Dictionary: ',len(_dict))
print('No of Collissions elements',ct)
print('Collision Track Variable Count:',collision_track)