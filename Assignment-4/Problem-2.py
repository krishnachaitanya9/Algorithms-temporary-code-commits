from random import getrandbits
import math

def partition(alist, start, end):

    def gen_random_number(n):
        bin_string = ''
        range_variable = 5*int((math.log(n) /
                math.log(2))+1)
        for _ in range(range_variable):
            bin_string+=str(getrandbits(1))
        if n-1 < int(bin_string,2):
            return n-1
        else:
            return int(bin_string,2)

    # pivot = int((start+end)/2)
    pivot = gen_random_number(len(alist))
    temp = alist[end]
    alist[end] = alist[pivot]
    alist[pivot] = temp
    pIndex = start

    for i in range(start, end):
        if getrandbits(1)==1:
            temp = alist[i]
            alist[i] = alist[pIndex]
            alist[pIndex] = temp
            pIndex += 1
    temp1 = alist[end]
    alist[end] = alist[pIndex]
    alist[pIndex] = temp1

    return pIndex

def quicksort(alist, start, end):
    '''This function calls the partition and then recurse itself using the index returned by partition'''
    if start < end:
        pIndex = partition(alist, start, end)
        quicksort(alist, start, pIndex - 1)
        quicksort(alist, pIndex + 1, end)

    return alist
def shuffleArray(a):
    temp = []
    temp.extend(a)
    return quicksort(temp,0,len(temp)-1)



import matplotlib.pyplot as plt


def placementTest(n, nTrials, elt):
    a = list(range(n))
    where = []
    for i in range(nTrials):
        b = shuffleArray(a)
        # find where element n/2 ended up
        j = b.index(elt)
        where.append(j)
    plt.figure("Position of elt. %d" % (elt))
    plt.hist(where, bins=80)
    plt.xlabel('Position of elt. %d' % (elt))
    plt.ylabel('Frequency')
    # TODO: Implement a xi-squared test
    # for now let us eyeball the histograms.
    # the histograms must be near uniform.


placementTest(30, 10000, 10)
placementTest(25, 10000, 7)
placementTest(18, 10000, 0)
