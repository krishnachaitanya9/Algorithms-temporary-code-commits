from random import getrandbits
import math
def shuffleArray(a):

    global _dict
    _dict = {}
    temp_list = []
    def gen_random_number(n):
        bin_string = ''
        global _dict
        range_variable = int((math.log(n) /
                              math.log(2)) + 1)
        for _ in range(range_variable):
            bin_string += str(getrandbits(1))
        num = int(bin_string, 2)
        if num not in _dict and num < n:
            _dict[num] = 1
            return num
        else:
            gen_random_number(n)
    for i in range(len(a)+1):
        gen_random_number(len(a))

    for rand_num in _dict:
        temp_list.append(a[rand_num])
    return temp_list


## Begin statistical tests
import matplotlib.pyplot as plt


def placementTest(n, nTrials, elt):
    a = list(range(n))
    where = []
    for i in range(nTrials):
        b = shuffleArray(a)
        # find where element n/2 ended up
        j = b.index(elt)
        where.append(j)
    plt.figure("Position of elt. %d" % (elt))
    plt.hist(where, bins=80)
    plt.xlabel('Position of elt. %d' % (elt))
    plt.ylabel('Frequency')
    # TODO: Implement a xi-squared test
    frequency = {}
    for index in where:
        frequency[index] = frequency.get(index, 0) + 1
    exp_frequency = nTrials / n
    chi_squared = 0
    for index in frequency:
        chi_squared += (frequency[index] - exp_frequency) ** 2 / exp_frequency
    print("Chi-Squared value for value ", n, " = ", round(chi_squared, 3))
    # for now let us eyeball the histograms.
    # the histograms must be near uniform.


placementTest(30, 10000, 10)
placementTest(25, 10000, 7)
placementTest(18, 10000, 0)
# placementTest(18, 10000, 17)

##  End statistical tests

# def gen_random_number2(n):
#     range_variable = math.ceil((math.log(n) / math.log(2)))
#     ind = []
#     _dict = {}
#     s = 0
#     while (s < n):
#         num = 0
#         for i in range(range_variable):
#             num = num + 2 ^ i * getrandbits(1)
#         if (num < n and num not in _dict):
#             _dict[num] = 1
#             ind.append(num)
#             s += 1
#     return ind
#
#
# print(gen_random_number2(10))
#
#
# def randomizer2(a):
#     for index, elem in enumerate(a):
#         rand = gen_random_number(len(a))
#         a[rand], a[index] = a[index], a[rand]
#     return a
#
#     def gen_random_number(n):
#         bin_string = ''
#         range_variable = int((math.log(n) /
#                               math.log(2)) + 1)
#         for _ in range(range_variable):
#             bin_string += str(getrandbits(1))
#         if int(bin_string, 2) < n:
#             return int(bin_string, 2)
#         else:
#             return gen_random_number(n)
