from random import getrandbits
import math


def randomizer(shufflist, start, end):
    def gen_random_number(n):
        bin_string = ''
        range_variable = int((math.log(n) /
                              math.log(2)) + 1)
        for _ in range(range_variable):
            bin_string += str(getrandbits(1))
        if int(bin_string, 2) < n:
            return int(bin_string, 2)
        else:
            return gen_random_number(n)

    def partition(shufflist, start, end):
        # pivot = int((start+end)/2)
        pivot = gen_random_number(len(shufflist))
        shufflist[end], shufflist[pivot] = shufflist[pivot], shufflist[end]
        pIndex = start

        for i in range(start, end):
            if getrandbits(1) == 1:
                shufflist[i], shufflist[pIndex] = shufflist[pIndex], shufflist[i]
                pIndex += 1
        shufflist[end], shufflist[pIndex] = shufflist[pIndex], shufflist[end]
        return pIndex


    if start < end:
        pIndex = partition(shufflist, start, end)
        randomizer(shufflist, start, pIndex - 1)
        randomizer(shufflist, pIndex + 1, end)
    return shufflist


def shuffleArray(a):
    temp = []
    temp.extend(a)
    return randomizer(temp, 0, len(temp) - 1)
#     return randomizer(randomizer(temp,0,len(temp)-1),0,len(temp)-1)

import matplotlib.pyplot as plt

def placementTest(n, nTrials, elt):
    a = list(range(n))
    where = []
    for i in range(nTrials):
        b = shuffleArray(a)
        # find where element n/2 ended up
        j = b.index(elt)
        where.append(j)
    plt.figure("Position of elt. %d"%(elt))
    plt.hist(where, bins=80)
    plt.xlabel('Position of elt. %d'%(elt))
    plt.ylabel('Frequency')
    # TODO: Implement a xi-squared test
    frequency = {}
    for index in where:
        frequency[index] = frequency.get(index, 0) + 1
    exp_frequency = nTrials/n
    chi_squared = 0
    for index in frequency:
        chi_squared += (frequency[index] - exp_frequency)**2/exp_frequency
    print("Chi-Squared value for value ",n," = ",round(chi_squared,3))
    # for now let us eyeball the histograms.
    # the histograms must be near uniform.

placementTest(30, 10000, 10)
placementTest(25, 10000, 7)
placementTest(18, 10000, 0)