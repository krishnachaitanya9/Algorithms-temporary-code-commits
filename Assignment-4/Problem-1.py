global count
count = 0
def findMinimum(a):
    global count
    n = len(a)
    minSoFar = math.inf
    for i in range(n):
        if (a[i] < minSoFar):
            minSoFar = a[i]  # <-- X
            count += 1
    return minSoFar

from random import getrandbits
import math
def shuffleArray(a):
    temp = []
    temp1 = []
    temp.extend(a)
    mydict = {}
    def gen_random_number(n):
        bin_string = ''
        range_variable = 5*int((math.log(n) /
                math.log(2))+1)
        for _ in range(range_variable):
            bin_string+=str(getrandbits(1))
        return int(bin_string,2)


    for elem in temp:
        mydict[gen_random_number(len(temp))] = elem

    for key,value in mydict.items():
        temp1.append(value)
    return temp1


def placementTest(n, nTrials, elt):
    a = list(range(n))
    for i in range(nTrials):
        b = shuffleArray(a)
        findMinimum(b)

# placementTest(30, 10000, 10)
placementTest(25, 10000, 7)
# placementTest(18, 10000, 0)
print('Expected no of times the line is run: ',float(count/10000))